mod canonical_json;
pub mod ubase64;
mod sign;
mod verify;

pub use sign::*;
pub use verify::*;

#[cfg(test)]
mod tests {
    use crate::signatures;
    use failure::format_err;
    use lazy_static::lazy_static;
    /// Tests the vectors from the spec appendix https://matrix.org/docs/spec/appendices.html#json-signing
    use signatures::{ubase64, Sign, Verify};
    use serde_json::json;
    use sodiumoxide::crypto::sign::ed25519;

    lazy_static! {
        static ref TEST_VEC_KEYPAIR: ed25519_dalek::Keypair = {
            // ed25519-dalek does not implement key generation from a seed, so we use libsodium's key generation
            let seed = ed25519::Seed::from_slice(
                &ubase64::decode("YJDBA9Xnr2sVqXD9Vj7XVUnmFZcZrlw8Md7kMW+3XA1").unwrap(),
            )
                .unwrap();
            let (public, secret) = ed25519::keypair_from_seed(&seed);

            let keypair = ed25519_dalek::Keypair {
                // sodiumoxide does not expose a Deref<[u8]> for SecretKey, but it does expose a Index<Range>.
                // secret[..] gives some 64-byte value, possibly what dalek calls an ExpandedSecretKey?
                secret: ed25519_dalek::SecretKey::from_bytes(&secret[..ed25519_dalek::SECRET_KEY_LENGTH]).unwrap(),
                public: ed25519_dalek::PublicKey::from_bytes(public.as_ref()).unwrap(),
            };

            // Sanity check: do both libraries sign "abc" the same with this key?
            let test_string = "abc".as_bytes();
            let dalek_sig = keypair.sign(test_string).to_bytes();
            let nacl_sig = &ed25519::sign_detached(test_string, &secret)[..];
            assert_eq!(nacl_sig, &dalek_sig[..]);

            keypair
        };

        static ref TEST_VEC_SIGNING_KEY: signatures::SigningKey<'static, 'static> = {
            signatures::SigningKey::new(TEST_VEC_KEYPAIR.public.as_bytes(), TEST_VEC_KEYPAIR.secret.as_bytes(), "domain", "1")
        };

        static ref TEST_VEC_VERIFICATION_KEY: signatures::VerificationKey = {
            signatures::VerificationKey::new_from_ed25519_key(TEST_VEC_KEYPAIR.public)
        };
    }

    fn test_vec_key_lookup_function(key_name: &str) -> Result<&signatures::VerificationKey, failure::Error> {
        if key_name == "ed25519:1" {
            Ok(&TEST_VEC_VERIFICATION_KEY)
        } else {
            Err(format_err!("no such key"))
        }
    }

    #[test]
    fn test_sign_vector_empty() {
        assert_eq!(
            json!({}).to_signed_json(&TEST_VEC_SIGNING_KEY).unwrap(),
            json!({
                "signatures": {
                    "domain": {
                        "ed25519:1": "K8280/U9SSy9IVtjBuVeLr+HpOB4BQFWbg+UZaADMtTdGYI7Geitb76LTrr5QV/7Xg4ahLwYGYZzuHGZKM5ZAQ"
                    }
                }
            })
        );
    }

    #[test]
    fn test_sign_vector_onetwo() {
        assert_eq!(
            json!({
                "one": 1,
                "two": "Two"
            })
                .to_signed_json(&TEST_VEC_SIGNING_KEY)
                .unwrap(),
            json!({
                "one": 1,
                "signatures": {
                    "domain": {
                        "ed25519:1": "KqmLSbO39/Bzb0QIYE82zqLwsA+PDzYIpIRA2sRQ4sL53+sN6/fpNSoqE7BP7vBZhG6kYdD13EIMJpvhJI+6Bw"
                    }
                },
                "two": "Two"
            })
        );
    }

    #[test]
    fn test_verify_vector_empty() {
        assert!(
            json!({
                "signatures": {
                    "domain": {
                        "ed25519:1": "K8280/U9SSy9IVtjBuVeLr+HpOB4BQFWbg+UZaADMtTdGYI7Geitb76LTrr5QV/7Xg4ahLwYGYZzuHGZKM5ZAQ"
                    }
                }
            }).verify_json("domain", test_vec_key_lookup_function).is_ok()
        );
    }

    #[test]
    fn test_verify_vector_empty_corrupted() {
        assert!(
            json!({
                "signatures": {
                    "domain": {
                        "ed25519:1": "K8280/U9SSy9IVtjBuVeLr+HpOB4BQFWbg+UZaADMtTdGYI7Geitb76LTrr5QV/7Xg4ahLwYGYZzuHGZKM5ZAy"
                    }
                }
            }).verify_json("domain", test_vec_key_lookup_function).is_err()
        );
    }

    #[test]
    fn test_verify_vector_no_key() {
        assert!(
            json!({
                "signatures": {
                    "domain": {
                        "ed25519:1": "K8280/U9SSy9IVtjBuVeLr+HpOB4BQFWbg+UZaADMtTdGYI7Geitb76LTrr5QV/7Xg4ahLwYGYZzuHGZKM5ZAQ"
                    }
                }
            }).verify_json("domain", |_| Err(format_err!("nope"))).is_err()
        );
    }

    #[test]
    fn test_verify_vector_onetwo() {
        assert!(
            json!({
                "one": 1,
                "signatures": {
                    "domain": {
                        "ed25519:1": "KqmLSbO39/Bzb0QIYE82zqLwsA+PDzYIpIRA2sRQ4sL53+sN6/fpNSoqE7BP7vBZhG6kYdD13EIMJpvhJI+6Bw"
                    }
                },
                "two": "Two"
            }).verify_json("domain", test_vec_key_lookup_function).is_ok()
        );
    }
}
