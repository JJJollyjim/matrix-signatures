/// Implements Unpadded Base64 as defined in https://matrix.org/docs/spec/appendices.html#unpadded-base64
use lazy_static::lazy_static;

lazy_static! {
    static ref BASE64_CONFIG: base64::Config =
        base64::Config::new(base64::CharacterSet::Standard, false).decode_allow_trailing_bits(true);
}

/// Encodes the given unpadded or padded base64 string
pub fn decode(val: &str) -> Result<Vec<u8>, base64::DecodeError> {
    base64::decode_config(val, *BASE64_CONFIG)
}

/// Encodes the given byte slice reference in accordance with the spec
pub fn encode(val: &[u8]) -> String {
    base64::encode_config(val, *BASE64_CONFIG)
}

#[cfg(test)]
mod tests {
    #[test]
    fn encode_matches_spec() {
        // Test vectors from spec
        assert_eq!(super::encode(b""), "");
        assert_eq!(super::encode(b"f"), "Zg");
        assert_eq!(super::encode(b"fo"), "Zm8");
        assert_eq!(super::encode(b"foo"), "Zm9v");
        assert_eq!(super::encode(b"foob"), "Zm9vYg");
        assert_eq!(super::encode(b"fooba"), "Zm9vYmE");
        assert_eq!(super::encode(b"foobar"), "Zm9vYmFy");
    }
}
