use failure::{format_err, Fail};
use serde_json::Value;
use log::*;
use super::{ubase64, canonical_json};

pub enum VerificationKey {
    Ed25519(ed25519_dalek::PublicKey),
}

impl VerificationKey {
    pub fn new(alg_id: &str, key_material: &[u8]) -> Result<Self, failure::Error> {
        match alg_id {
            "ed25519" => Ok(VerificationKey::Ed25519(ed25519_dalek::PublicKey::from_bytes(key_material)?)),
            _ => Err(format_err!("Unknown signature algorithm {}", alg_id))
        }
    }

    pub fn new_from_ed25519_key(key: ed25519_dalek::PublicKey) -> Self {
        VerificationKey::Ed25519(key)
    }

    fn _alg_id(&self) -> &'static str {
        match self {
            VerificationKey::Ed25519(_) => "ed25519",
        }
    }

    fn alg_known(alg: &str) -> bool{
        alg == "ed25519"
    }

    fn verify(&self, data: &[u8], signature: &[u8]) -> Result<(), failure::Error> {
        match self {
            VerificationKey::Ed25519(key) => key.verify(data, &ed25519_dalek::Signature::from_bytes(signature)?)?
        };
        Ok(())
    }
}

fn split2(s: &str, pat: char) -> Option<(&str, &str)> {
    let mut iter = s.split(pat);

    let fst = iter.next()?;
    let snd = iter.next()?;

    if iter.next().is_some() {
        None?
    }

    Some((fst, snd))
}

#[derive(Debug, Fail)]
#[fail(display = "error looking up key")]
struct KeyLoookupError(#[cause] failure::Error);

pub trait Verify: Sized {
    fn as_value(self) -> Value;

    fn verify_json(self, domain: &str, mut key_lookup: impl FnMut(&str) -> Result<&VerificationKey, failure::Error>) -> Result<(), failure::Error> {
        let mut val = self.as_value();

        val.as_object_mut().ok_or_else(|| format_err!("Value to be verified is not an object"))?.remove("unsigned");

        val.as_object_mut()
            .unwrap()
            .remove("signatures")
            .ok_or_else(|| format_err!("Object to be verified has no signatures"))?
            .as_object()
            .ok_or_else(|| format_err!("Signatures is not an object"))?
            .get(domain)
            .ok_or_else(|| format_err!("No signatures for domain"))?
            .as_object()
            .ok_or_else(|| format_err!("Signatures for domain {} is not an object", domain))?
            .iter()
            .find_map(|(alg_and_id, sig_str)| {
                let result = (|| -> Result<(), failure::Error> {
                    let (alg, _id) = split2(alg_and_id, ':').ok_or_else(|| format_err!("Bad key alg:id"))?;

                    if !VerificationKey::alg_known(alg) {
                        Err(format_err!("Unknown algorithm {}", alg))?;
                    }

                    let key = key_lookup(alg_and_id).map_err(KeyLoookupError)?;

                    let signature = ubase64::decode(sig_str.as_str().ok_or_else(|| format_err!("Key is not a string"))?)?;

                    let encoded = canonical_json::encode_value(&val)?;

                    key.verify(encoded.as_bytes(), &signature)?;

                    Ok(())
                })();

                match result {
                    Ok(()) => Some(()),
                    Err(e) => {
                        info!("Skipping signature {}: {}", alg_and_id, e);
                        None
                    },
                }
            }).ok_or_else(|| format_err!("No good signature found"))
    }

}

impl Verify for Value {
    fn as_value(self) -> Value {
        self
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_split2() {
        assert_eq!(super::split2("abc:def", ':'), Some(("abc", "def")));
        assert_eq!(super::split2("abcdefg", ':'), None);
        assert_eq!(super::split2("a:bcd:e", ':'), None);
    }
}
