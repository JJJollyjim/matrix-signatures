use serde_json::{Value, to_value, Map};
use super::{ubase64, canonical_json};

pub struct SigningKey<'a, 'b> {
    keypair: SigningKeyPair,
    domain: &'a str,
    id: &'b str,
}

impl<'a, 'b> SigningKey<'a, 'b> {
    pub fn new(
        public_bytes: &[u8],
        secret_bytes: &[u8],
        domain: &'a str,
        id: &'b str,
    ) -> SigningKey<'a, 'b> {
        SigningKey {
            domain,
            id,
            keypair: SigningKeyPair::Ed25519(ed25519_dalek::Keypair {
                secret: ed25519_dalek::SecretKey::from_bytes(secret_bytes).unwrap(),
                public: ed25519_dalek::PublicKey::from_bytes(public_bytes).unwrap(),
            }),
        }
    }

    pub fn new_from_ed25519_pair(
        pair: ed25519_dalek::Keypair,
        domain: &'a str,
        id: &'b str,
    ) -> SigningKey<'a, 'b> {
        SigningKey {
            domain,
            id,
            keypair: SigningKeyPair::Ed25519(pair),
        }
    }
}

enum SigningKeyPair {
    Ed25519(ed25519_dalek::Keypair),
}

impl SigningKeyPair {
    fn alg_id(&self) -> &'static str {
        match self {
            SigningKeyPair::Ed25519(_) => "ed25519",
        }
    }

    fn sign(&self, data: &[u8]) -> String {
        ubase64::encode(&match self {
            SigningKeyPair::Ed25519(pair) => pair.sign(data).to_bytes(),
        })
    }
}

pub trait Sign: serde::Serialize + Sized {
    fn to_signed_json(self, key: &SigningKey) -> Result<Value, serde_json::Error> {

        let mut val = to_value(self)?;
        let (unsigned, mut signatures) = {
            let val_map = val
                .as_object_mut()
                .expect("Types implementing Sign must serialize as maps!");

            (
                val_map.remove("unsigned"),
                val_map
                    .remove("signatures")
                    .unwrap_or_else(|| Value::Object(Map::new())),
            )
        };

        let canon = canonical_json::encode_value(&val)?;

        let sig = key.keypair.sign(&canon.as_bytes());
        let signatures_map = signatures
            .as_object_mut()
            .expect("Types implementing Sign which have a signatures field must serialize signatures as a map!");

        let domain_sigs = signatures_map.entry(key.domain).or_insert(Value::Object(Map::new()))
            .as_object_mut()
            .expect("Types implementing Sign which have a signatures field must serialize the values of the signatures map as maps!");

        domain_sigs.insert(
            format!("{}:{}", key.keypair.alg_id(), key.id),
            Value::String(sig),
        );

        let val_map = val.as_object_mut().unwrap();

        if let Some(data) = unsigned {
            val_map.insert("unsigned".to_owned(), data);
        }

        val_map.insert("signatures".to_owned(), signatures);

        Ok(val)
    }
}

impl Sign for Value {}
