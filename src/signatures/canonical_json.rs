/// Implements Canonical JSON as defined in https://matrix.org/docs/spec/appendices.html#canonical-json
use serde::Serialize;

pub fn encode<T: Serialize>(x: &T) -> Result<String, serde_json::Error> {
    // Value uses a BTreeMap to represent maps, which may mean this is all we need to implement canonical json?
    // Not super efficient though, with all the allocations to make a Value tree
    let val = serde_json::to_value(x)?;
    encode_value(&val)
}

/// `encode` converts the argument to a Value, which is probably slow: this is
/// an optimized path for when we already have a Value
pub fn encode_value(x: &serde_json::Value) -> Result<String, serde_json::Error> {
    serde_json::to_string(x)
}

#[cfg(test)]
mod tests {
    fn assert_reencoding_is(pretty: &str, correct: &str) {
        assert_eq!(
            super::encode(&serde_json::from_str::<serde_json::Value>(pretty).unwrap()).unwrap(),
            correct
        );
    }

    #[test]
    fn test_vectors_reencode_correctly() {
        assert_reencoding_is(r#"{}"#, r#"{}"#);

        assert_reencoding_is(
            r#"{
    "one": 1,
    "two": "Two"
}"#,
            r#"{"one":1,"two":"Two"}"#,
        );

        assert_reencoding_is(
            r#"{
    "b": "2",
    "a": "1"
}"#,
            r#"{"a":"1","b":"2"}"#,
        );

        assert_reencoding_is(r#"{"b":"2","a":"1"}"#, r#"{"a":"1","b":"2"}"#);

        assert_reencoding_is(r#"{
    "auth": {
        "success": true,
        "mxid": "@john.doe:example.com",
        "profile": {
            "display_name": "John Doe",
            "three_pids": [
                {
                    "medium": "email",
                    "address": "john.doe@example.org"
                },
                {
                    "medium": "msisdn",
                    "address": "123456789"
                }
            ]
        }
    }
}"#, r#"{"auth":{"mxid":"@john.doe:example.com","profile":{"display_name":"John Doe","three_pids":[{"address":"john.doe@example.org","medium":"email"},{"address":"123456789","medium":"msisdn"}]},"success":true}}"#);

        assert_reencoding_is(
            r#"{
    "a": "日本語"
}"#,
            r#"{"a":"日本語"}"#,
        );

        assert_reencoding_is(
            r#"{
    "本": 2,
    "日": 1
}"#,
            r#"{"日":1,"本":2}"#,
        );

        assert_reencoding_is(
            r#"{
    "a": "\u65E5"
}"#,
            r#"{"a":"日"}"#,
        );

        assert_reencoding_is(
            r#"{
    "a": null
}"#,
            r#"{"a":null}"#,
        );
    }

    #[test]
    fn test_many_hashmaps_encode_in_order() {
        use std::collections::HashMap;

        // At time of writing, each newly created HashMap has a different
        // iteration order, perfect for this test
        for _ in 1..100 {
            let mut map = HashMap::new();
            map.insert("a", ());
            map.insert("c", ());
            map.insert("\0", ());
            map.insert("b", ());

            assert_eq!(
                super::encode(&map).unwrap(),
                r#"{"\u0000":null,"a":null,"b":null,"c":null}"#
            );
        }
    }
}
