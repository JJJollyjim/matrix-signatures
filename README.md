An implementation of Matrix's JSON signing spec (and its dependencies Unpadded Base64 and Canonical JSON) in Rust.

Fully functional, tests pass for all test vectors in the [spec](https://matrix.org/docs/spec/appendices.html#json-signing).

The first step in my experiments with implementing Matrix federation.

## License

> Matrix federation experiments
> Copyright (C) 2019 Jamie McClymont \<jamie@kwiius.com\>
>
> This program is free software: you can redistribute it and/or modify
> it under the terms of the GNU Affero General Public License as
> published by the Free Software Foundation, either version 3 of the
> License, or (at your option) any later version.
>
> This program is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
> GNU Affero General Public License for more details.
>
> You should have received a copy of the GNU Affero General Public License
> along with this program.  If not, see \<http://www.gnu.org/licenses/\>.
